/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Pressable,
  Linking,
  TouchableOpacity,
} from 'react-native';

import QRCodeScanner from 'react-native-qrcode-scanner';
import {RNCamera} from 'react-native-camera';
import base64 from 'react-native-base64';
const App: () => React$Node = () => {
  const [enabled, setEnabled] = useState(false);
  const [message, setMessage] = useState('');

  function base64ArrayBuffer(arrayBuffer) {
    var base64 = '';
    var encodings =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

    var bytes = new Uint8Array(arrayBuffer);
    var byteLength = bytes.byteLength;
    var byteRemainder = byteLength % 3;
    var mainLength = byteLength - byteRemainder;

    var a, b, c, d;
    var chunk;

    // Main loop deals with bytes in chunks of 3
    for (var i = 0; i < mainLength; i = i + 3) {
      // Combine the three bytes into a single integer
      chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];

      // Use bitmasks to extract 6-bit segments from the triplet
      a = (chunk & 16515072) >> 18; // 16515072 = (2^6 - 1) << 18
      b = (chunk & 258048) >> 12; // 258048   = (2^6 - 1) << 12
      c = (chunk & 4032) >> 6; // 4032     = (2^6 - 1) << 6
      d = chunk & 63; // 63       = 2^6 - 1

      // Convert the raw binary segments to the appropriate ASCII encoding
      base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
    }

    // Deal with the remaining bytes and padding
    if (byteRemainder == 1) {
      chunk = bytes[mainLength];

      a = (chunk & 252) >> 2; // 252 = (2^6 - 1) << 2

      // Set the 4 least significant bits to zero
      b = (chunk & 3) << 4; // 3   = 2^2 - 1

      base64 += encodings[a] + encodings[b] + '==';
    } else if (byteRemainder == 2) {
      chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];

      a = (chunk & 64512) >> 10; // 64512 = (2^6 - 1) << 10
      b = (chunk & 1008) >> 4; // 1008  = (2^6 - 1) << 4

      // Set the 2 least significant bits to zero
      c = (chunk & 15) << 2; // 15    = 2^4 - 1

      base64 += encodings[a] + encodings[b] + encodings[c] + '=';
    }

    return base64;
  }
  const onSuccess = (e) => {
    console.log(e);
    var str = e.data;
    var bytes = [];
    var bytesv2 = [];
    for (var i = 0; i < str.length; ++i) {
      var code = str.charCodeAt(i);
      bytes = bytes.concat([code]);
      bytesv2 = bytesv2.concat([code & 0xff, (code / 256) >>> 0]);
    }
    console.log(bytes);
    console.log(bytesv2);
    // 72, 101, 108, 108, 111, 31452
    console.log('bytes', bytes.join(', '));
    // 72, 0, 101, 0, 108, 0, 108, 0, 111, 0, 220, 122
    console.log('bytesv2', bytesv2.join(', '));
    console.log(base64ArrayBuffer(bytes));
    setMessage(base64ArrayBuffer(bytes));
    setEnabled(false);
  };

  return (
    <View style={styles.body}>
      <Text style={styles.header}> Leitor QR code</Text>
      <Pressable style={styles.button} onPress={() => setEnabled(true)}>
        <Text style={styles.buttonText}> Abrir leitor QR</Text>
      </Pressable>
      <Text>mensagem: {message}</Text>

      {enabled && (
        <QRCodeScanner
          onRead={onSuccess}
          showMarker={true}
          flashMode={RNCamera.Constants.FlashMode.torch}
          bottomContent={
            <TouchableOpacity style={styles.buttonTouchable}>
              <Text style={styles.buttonText}>OK. Got it!</Text>
            </TouchableOpacity>
          }
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  body: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  buttonText: {
    fontWeight: 'bold',
    color: '#fff',
    fontSize: 18,
  },
  button: {
    backgroundColor: '#0288D1',
    width: '60%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    elevation: 6,
    margin: 20,
  },
});

export default App;
